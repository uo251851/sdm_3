package algorithm;

public class Game {
	
	static Board board;

	public static void main(String[] args) {
		if (args.length==0)
			throw new RuntimeException("No arguments");
		//Create board, dim 8 by default
		board = new Board(Integer.parseInt(args[0]));
		
		//Place the initial void, random by default
		if (args.length==1)
			board.placeBlank();
		else if (args.length==3)
			board.placeBlank(Integer.parseInt(args[1]), Integer.parseInt(args[2]));
		else
			throw new RuntimeException("Not correct arguments");
		
		long t1 = System.currentTimeMillis();
		
		
		board.recursiveSolve();
		long t2 = System.currentTimeMillis();
		
		System.out.println(t2-t1);
		board.printPuzzle();
		
		System.out.println("Elapsed time: " +(t2-t1));
	}
}
