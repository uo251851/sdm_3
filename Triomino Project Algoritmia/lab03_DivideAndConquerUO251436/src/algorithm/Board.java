package algorithm;

import java.util.Random;

public class Board {
	
	boolean[][]  tiles;
	int dimension;
	
	
	public Board (int dimension)
	{
		tiles = new boolean[dimension][dimension];
		this.dimension = dimension;
		
	}


	public void placeBlank() {
		Random random = new Random();
		tiles[random.nextInt(dimension)][random.nextInt(dimension)] = true;
		
	}

	public void placeBlank(int a, int b) {
		tiles[a][b] = true;
		
	}
	
	public int checkQuadrantBlank(int c1, int c2, int dimension)
	{
		int[] res = checkTileBlank(c1, c2, dimension);
		if (res[0]>=dimension)
			res[0]-=dimension;
		if(res[1]>=dimension)
			res[1]-=dimension;
		if (res[0] < dimension/2)
		{
			if(res[1] < dimension/2)
			{
		//		System.out.println("The quadrant containing the tile is 1");
				return 1;
			}
		//	System.out.println("The quadrant containing the tile is 3");

			return 3;
		}
		else
		{
			if (res[0] >= dimension/2)
				if(res[1] < dimension/2)
				{
		//			System.out.println("The quadrant containing the tile is 2");

					return 2;
				}
		//	System.out.println("The quadrant containing the tile is 4");
			return 4;
		}

	}
	
	private int[] checkTileBlank(int c1, int c2, int dimension)
	{
		int[] ret = new int[2];
		
		for (int i=c1; i< c1+(dimension); i++)
			for (int j =c2; j< c2+(dimension); j++)
			{
				if (tiles[i][j])
				{
					ret[0] = i;
					ret[1] = j;
					break;
				}
			}
//		System.out.println();
//		System.out.println("Found starting tile in " + ret[0] + " ," + ret[1]);
//		System.out.println();
		return ret; 
			

	}
	
	public void placeFirstTromino(int blankQuadrant,int c1, int c2, int dimension)
	{
		if (blankQuadrant == 1)
		{
			tiles[dimension/2+c1][dimension/2 -1+c2] = true;
			tiles[dimension/2+c1][dimension/2+c2] = true;
			tiles[dimension/2 -1+c1][dimension/2+c2] = true;
		}
		
		else if (blankQuadrant == 2)
		{
			tiles[dimension/2-1+c1][dimension/2 -1+c2] = true;
			tiles[dimension/2 -1+c1][dimension/2+c2] = true;
			tiles[dimension/2+c1][dimension/2+c2] = true;
		}
		
		else if (blankQuadrant == 3)
		{
			tiles[dimension/2-1+c1][dimension/2 -1+c2] = true;
			tiles[dimension/2+c1][dimension/2 -1+c2] = true;
			tiles[dimension/2+c1][dimension/2+c2] = true;
		}
		
		else
		{
			tiles[dimension/2-1+c1][dimension/2 -1+c2] = true;
			tiles[dimension/2 -1+c1][dimension/2+c2] = true;
			tiles[dimension/2+c1][dimension/2 -1+c2] = true;
		}
		
	}


	public void recursiveSolve()
	{
		solve(0,0, this.dimension);
	}
		
	private void solve(int coord1, int coord2, int dimension)
	{
			
		if (dimension== 2)
		{
			solveBasicCase(coord1,coord2);
			System.out.println();
			System.out.println();
			System.out.println("Solved simple case:");
			printPuzzle();
			System.out.println();
		}
		else if (dimension > 2)
		{
			
			placeFirstTromino(checkQuadrantBlank(coord1, coord2, dimension),coord1,coord2, dimension);
			
			System.out.println();
			System.out.println("Placed First Tromino:");
			System.out.println();
			printPuzzle();
			
			solve(coord1,coord2,dimension/2); //Upper Left
			
			solve(coord1+dimension/2, coord2,dimension/2);	
			
			solve(coord1, coord2+dimension/2,dimension/2);	//Down Left
	
			solve(coord1+dimension/2, coord2+dimension/2,dimension/2); 
		
		}
			

	}
	
	private void solveBasicCase  (int coord1, int coord2)
	{
		for (int i=coord1; i< coord1+2; i++)
			for (int j =coord2; j< coord2+2; j++)
			{
				if (tiles[i][j] == false)
				{
					tiles[i][j] = true;
				}
			}
			
	}
	
	public void printPuzzle()
	{
		for (int i=0; i<tiles[0].length; i++)
		{
				System.out.println();
			for (int j=0; j<tiles[0].length; j++)
			{
				
				if (tiles[i][j]==true)
					System.out.print("T ");
				else
					System.out.print("F ");
			}
		}
	}
}	
