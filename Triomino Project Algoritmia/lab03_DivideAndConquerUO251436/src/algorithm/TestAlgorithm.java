package algorithm;

public class TestAlgorithm {

	static int n = 2;
	public static void main(String[] args) {
		while (true)
		{
			testTimes(n);
			n*=2;
		}
	}

	
	private static void testTimes(int n)
	{
		Board board = new Board(n);
		board.placeBlank();
		
		long t1 = System.currentTimeMillis();

		board.recursiveSolve();
		
		long t2 = System.currentTimeMillis();

		System.out.println("N = "+ n + " ,elapsed time: " +(t2-t1));
	}
}
