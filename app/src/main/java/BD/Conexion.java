package BD;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import Logica.Usuario;

/**
 * Created by AlexGonzCam on 31/12/17.
 */

public class Conexion {

    final static FirebaseDatabase database = FirebaseDatabase.getInstance();


    public Conexion(){

    }

    public  static DatabaseReference usuariosConexion(){
        return database.getReference(FirebaseReferences.USUARIOSINFO);
    }

    public void añadirUsuario(Usuario usuario){
        DatabaseReference usuariosref = usuariosConexion();
        usuariosref.child(usuario.getUsername()).setValue(usuario);

    }

    public void actualizarUsuario(Usuario usuario){
        DatabaseReference usuariosref = usuariosConexion();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String email = FirebaseAuth.getInstance().getCurrentUser().getEmail().toString();
        usuariosref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                Usuario u = dataSnapshot.getValue(Usuario.class);
                if(u.getEmail().equals(email)) {

                    if(u.getEmail().equals(usuario.getEmail())){
                        usuario.setPuntuacion(u.getPuntuacion());
                        usuario.setImg(u.getImg());
                        usuario.setUsername(u.getUsername());
                        usuariosref.child(usuario.getUsername()).setValue(usuario);
                    }else{
                        usuario.setPuntuacion(u.getPuntuacion());
                        usuario.setImg(u.getImg());
                        usuario.setUsername(u.getUsername());
                        usuariosref.child(usuario.getUsername()).setValue(usuario);
                        FirebaseAuth.getInstance().getCurrentUser().updateEmail(usuario.getEmail());



                    }


                }



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
