package com.example.ragna.trominos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ragna.trominos.Opciones.*;

public class DificultadActivity extends AppCompatActivity {

    private String modojuego;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dificultad);

        Intent intent = getIntent();
        String modojuego = intent.getStringExtra("modo");
        this.modojuego = modojuego;

        if (modojuego.equals("contrarreloj"))
        {
            ImageView imageCronometro = (ImageView) findViewById(R.id.imageCronometro);
            imageCronometro.setVisibility(View.VISIBLE);
        }
    }

    public void jugarFacil(View view) {
        jugar(view, ModosJuegoActivity.FACIL);
    }

    public void jugarNormal(View view) {
        jugar(view, ModosJuegoActivity.NORMAL);
    }

    public void jugarDificil(View view) {
        jugar(view, ModosJuegoActivity.DIFICIL);
    }

    /**
     * Para preparar la menu es necesario conocer la dificultad seleccionada por el usuario.
     *
     * @param view
     * @param dificultad
     */
    private void jugar(View view, int dificultad) {
        Intent jugar = new Intent(DificultadActivity.this, PartidaActivity.class);
        jugar.putExtra("dificultad", dificultad);
        jugar.putExtra("modo", modojuego);
        startActivity(jugar);
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }

}
