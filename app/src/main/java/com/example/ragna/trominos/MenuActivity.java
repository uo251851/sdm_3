package com.example.ragna.trominos;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import Logica.ConexionInternet;

public class MenuActivity extends AppCompatActivity {


    private TextView email;
    private TextView contraseña;
    private ConexionInternet internet = new ConexionInternet();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);



        email = (TextView) findViewById(R.id.email);
        contraseña = (TextView) findViewById(R.id.contraseña);


    }
    public void LogIn(View view){
        //Validaciones
        if(internet.isOnlineNet()){
            String email1 = email.getText().toString().trim();
            String pass = contraseña.getText().toString().trim();
            if(!email1.isEmpty() && !pass.isEmpty()){
                login(email1,pass);
            }else{
                Toast.makeText(MenuActivity.this,"Campos vacíos",Toast.LENGTH_SHORT).show();

            }
        }
        else
            Toast.makeText(MenuActivity.this,"Sin conexion a internet.",Toast.LENGTH_SHORT).show();


    }



    private void login(String email,String contraseña){

        FirebaseAuth.getInstance().signInWithEmailAndPassword(email,contraseña).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isSuccessful()){
                    Toast.makeText(MenuActivity.this,"E-mail o contraseña no validos.",Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent modosdejuego = new Intent(MenuActivity.this,ModosJuegoActivity.class);
                    startActivity(modosdejuego);
                    finish();
                }
            }
        });
    }

    public void Registrar(View view) {
        if(internet.isOnlineNet()){
            Intent registrar = new Intent(MenuActivity.this,RegistrarActivity.class);
            startActivity(registrar);
        }else
            Toast.makeText(MenuActivity.this,"Sin conexion a internet.",Toast.LENGTH_SHORT).show();

    }


    public void Invitado(View view) {
        Intent modosdejuego = new Intent(MenuActivity.this,ModosJuegoActivity.class);
        startActivity(modosdejuego);
    }




}
