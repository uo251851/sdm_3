package com.example.ragna.trominos;

import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import BD.Conexion;
import BD.FirebaseReferences;
import Logica.ConexionInternet;
import Logica.Usuario;
import Logica.adapter.UsuarioAdapter;

public class FinalActivity extends AppCompatActivity {

    private int dificultad;
    private String modojuego;
    private FinalActivity activity = this;
    private int bonificacion;
    private int puntos;
    private int total;
    private int tiempoRestante;
    private TextView txfinalizado;
    private TextView txpuntos;
    private ListView ranking;
    private int tiempoTotal;

    private ConexionInternet internet = new ConexionInternet();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);

        Intent intent = getIntent();
        int dificultad = intent.getIntExtra("dificultad", 0);
        this.dificultad = dificultad;
        String modojuego = intent.getStringExtra("modo");
        this.modojuego = modojuego;
        int puntos = intent.getIntExtra("puntos",0);
        this.puntos = puntos;
        int bonificacion = intent.getIntExtra("bonificacion",0);
        this.bonificacion = bonificacion;
        int tiempoRestante = intent.getIntExtra("tiemporestante",0);
        this.tiempoRestante = tiempoRestante;
        int tiempoTotal = intent.getIntExtra("tiempototal",0);
        this.tiempoTotal = tiempoTotal;

        this.total = bonificacion+puntos;
        this.txfinalizado =(TextView) findViewById(R.id.txFinalizado);
        this.txpuntos =(TextView) findViewById(R.id.txPuntuacion);
        this.ranking = (ListView) findViewById(R.id.listRanking);

        if (modojuego.equals("normal")) mostrarFinalNormal();
        else if (modojuego.equals("contrarreloj")) mostrarFinalContrarreloj();
        else if (modojuego.equals("torneo")) mostrarFinalTorneo();
    }

    private void mostrarFinalNormal() {
        ranking.setVisibility(View.GONE);
        txfinalizado.setText("¡Finalizado!");
        txpuntos.setText("Tiempo total: "+tiempoTotal+" seg.");
    }

    private void mostrarFinalContrarreloj() {
        ranking.setVisibility(View.GONE);
        if (tiempoRestante >= 50)
        {
            txfinalizado.setText("¡Muy bien hecho!");
        }
        else if (tiempoRestante<60 && tiempoRestante>=30)
        {
            txfinalizado.setText("¡Lo has hecho bastante bien!");
        }
        else if (tiempoRestante<30 && tiempoRestante>=5)
        {
            txfinalizado.setText("¡Sé que puedes hacerlo mejor!");
        }
        else if (tiempoRestante>0 && tiempoRestante<5)
        {
            txfinalizado.setText("¡Justo a tiempo!");
        }

        if (tiempoRestante !=0) txpuntos.setText("Tiempo restante: "+tiempoRestante+" seg.");
        else txpuntos.setText("Tienes que practicar un poco más.");
    }

    private void mostrarFinalTorneo() {
        Button btnMostrar = (Button) findViewById(R.id.btnVerRanking);
        btnMostrar.setEnabled(true);
        btnMostrar.setVisibility(View.VISIBLE);

        actualizarPuntuacion();
        actualizarRanking();

        String textofinal;
        if (total >= 1000)
            textofinal = "¡Eres de otro planeta!";
        else if (total < 1000 && total >= 500)
            textofinal = "¡Wow! Buen resultado amigx.";
        else
            textofinal = "¡La práctica hace la perfección! Sigue intentándolo.";

        txfinalizado.setText(textofinal);
        txpuntos.setText("Puntos obtenidos: "+puntos+"\nBonificiación: "+bonificacion+
                        "\nTotal: "+total);
    }

    public void mostrarRanking(View view)
    {
        List<Usuario> usuarios = new ArrayList<Usuario>();
        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        DatabaseReference usuariosref1 = database1.getReference(FirebaseReferences.USUARIOSINFO);
        usuariosref1.orderByChild("puntuacion").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                Usuario usuario = dataSnapshot.getValue(Usuario.class);
                usuarios.add(usuario);
                UsuarioAdapter adapter = new UsuarioAdapter(usuarios,activity);
                ranking.setAdapter(adapter);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        Button btnMostrar = (Button) findViewById(R.id.btnVerRanking);
        btnMostrar.setEnabled(false);
        btnMostrar.setVisibility(View.INVISIBLE);
        ranking.setVisibility(View.VISIBLE);
    }

    private void actualizarRanking()
    {
        if(internet.isOnlineNet()){
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference usuariosref = database.getReference(FirebaseReferences.USUARIOSINFO);
                usuariosref.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                        Usuario usuario = dataSnapshot.getValue(Usuario.class);
                        if(usuario.getEmail().equals(getEmail())){
                            usuario.setPuntuacion((total+usuario.getPuntuacion()*(-1))*(-1));
                            Conexion conn = new Conexion();
                            conn.añadirUsuario(usuario);
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {}

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
        }else{
            Toast.makeText(this,"No conexion a internet disponible",Toast.LENGTH_SHORT).show();

        }

    }

    private void actualizarPuntuacion()
    {
        if(internet.isOnlineNet()){
            FirebaseDatabase database = FirebaseDatabase.getInstance();

            DatabaseReference usuariosref = database.getReference(FirebaseReferences.USUARIOSINFO);
            usuariosref.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                    Usuario usuario = dataSnapshot.getValue(Usuario.class);
                    if(usuario.getEmail().equals(getEmail())) {
                        int puntuacion = total + (usuario.getPuntuacion()*(-1));
                        usuario.setPuntuacion(puntuacion*(-1));
                        Conexion conn = new Conexion();
                        conn.añadirUsuario(usuario);
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {}

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });
        }else{
            Toast.makeText(this,"No conexion a internet disponible",Toast.LENGTH_SHORT).show();

        }

    }

    private String getEmail()
    {
        return FirebaseAuth.getInstance().getCurrentUser().getEmail().toString();
    }

    public void nuevaPartida(View view) {
        Intent nuevapartida = new Intent(FinalActivity.this, PartidaActivity.class);
        nuevapartida.putExtra("dificultad", dificultad);
        nuevapartida.putExtra("modo", modojuego);
        startActivity(nuevapartida);
        finish();
    }

    public void volverAlMenu(View view) {
        Intent menu = new Intent(FinalActivity.this, ModosJuegoActivity.class);
        startActivity(menu);
        finish();
    }
}
