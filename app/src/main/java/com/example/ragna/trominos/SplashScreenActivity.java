package com.example.ragna.trominos;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Random;

import Logica.ConexionInternet;

public class SplashScreenActivity extends AppCompatActivity {

    public final static  int segs = new Random().nextInt(5-2)+2; //Entre 2 y 4 + delay segs de carga
    public final static int milisegs = segs*1000;
    public  static final int delay = 1;
    private ProgressBar pBar;
    private TextView textCargando;

    private FirebaseAuth.AuthStateListener mAuthListener;
    private ConexionInternet internet = new ConexionInternet();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        textCargando = (TextView) findViewById(R.id.textViewCargando);

        pBar = (ProgressBar) findViewById(R.id.progressBarSplashScreen);
        pBar.setMax(maxProgress());

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                RelativeLayout rL = (RelativeLayout) findViewById(R.id.logoLayout);
                rL.setBackgroundColor(Color.WHITE);
                if(internet.isOnlineNet()){
                    FirebaseUser user = firebaseAuth.getCurrentUser();

                    if(user!=null && (user.getEmail()!= null)){
                        //Inicia sesion
                        //StartActivity
                        Intent modosdejuego = new Intent(SplashScreenActivity.this,ModosJuegoActivity.class);
                        startActivity(modosdejuego);
                        finish();
                    }else{
                        Intent mainActivityIntent = new Intent(SplashScreenActivity.this, MenuActivity.class);
                        startActivity(mainActivityIntent);
                        finish();
                    }
                }else{
                    Toast.makeText(SplashScreenActivity.this,"No conexion a internet disponible",Toast.LENGTH_SHORT).show();
                    Intent mainActivityIntent = new Intent(SplashScreenActivity.this, MenuActivity.class);
                    startActivity(mainActivityIntent);
                    finish();

                }
            }
        };

        animate();
    }

    /**
     * Crea un countdown timer con la duración especificada en los atributos
     * y setea un tic por segundo

     */
    public void animate ()
    {
        new CountDownTimer(milisegs, 1000)
        {

            @Override
            public void onTick(long l) {

                pBar.setProgress(setBarProgress(1));

            }

            @Override
            public void onFinish() {
                RelativeLayout rL = (RelativeLayout) findViewById(R.id.logoLayout);
                rL.findViewById(R.id.progressBarSplashScreen).setVisibility(View.INVISIBLE);
                textCargando.setText(R.string.accediendoBD);
                FirebaseAuth.getInstance().addAuthStateListener(mAuthListener);
            }
        }.start();
    }

    public int setBarProgress (long milis)
    {
        return (int)((milisegs-milis)/1000);
    }

    public int maxProgress ()
    {
        return segs-delay;
    }



}
