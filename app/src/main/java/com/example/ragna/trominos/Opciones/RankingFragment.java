package com.example.ragna.trominos.Opciones;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ragna.trominos.R;
import com.example.ragna.trominos.RegistrarActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import BD.Conexion;
import BD.FirebaseReferences;
import Logica.ConexionInternet;
import Logica.Usuario;
import Logica.adapter.UsuarioAdapter;

/**
 * Created by AlexGonzCam on 1/1/18.
 */

public class RankingFragment extends Fragment {
    private static final String TAG = "RankingFragment";


    private ListView items;
    private List<Usuario> usuarios = new ArrayList<>();

    private ConexionInternet internet = new ConexionInternet();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.ranking_fragment,container,false);

        items = (ListView) view.findViewById(R.id.list);

        if(internet.isOnlineNet()){
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference usuariosref = database.getReference(FirebaseReferences.USUARIOSINFO);
            usuariosref.orderByChild("puntuacion").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                    Usuario usuario = dataSnapshot.getValue(Usuario.class);
                    usuarios.add(usuario);
                    UsuarioAdapter adapter = new UsuarioAdapter(usuarios,getContext());
                    items.setAdapter(adapter);

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else{
            Toast.makeText(getActivity(),"No conexion a internet disponible",Toast.LENGTH_SHORT).show();

        }



        return view;
    }


}
