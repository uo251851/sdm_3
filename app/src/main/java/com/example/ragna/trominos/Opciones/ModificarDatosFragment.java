package com.example.ragna.trominos.Opciones;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ragna.trominos.R;
import com.example.ragna.trominos.RegistrarActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import BD.Conexion;
import BD.FirebaseReferences;
import Logica.ConexionInternet;
import Logica.Usuario;
import Logica.Validador;

/**
 * Created by AlexGonzCam on 1/1/18.
 */

public class ModificarDatosFragment extends Fragment {
    private static final String TAG = "ModificarDatosFragment";

    private String emailantiguo;
    private TextView nombre;
    private TextView apellidos;
    private TextView email;
    private TextView contraseña;
    private TextView repcontraseña;

    private ConexionInternet internet = new ConexionInternet();





    public ModificarDatosFragment(String email) {
        this.emailantiguo = email;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.modificardatos_fragment,container,false);
        nombre = (TextView) view.findViewById(R.id.nombre);
        apellidos = (TextView) view.findViewById(R.id.apellidos);
        email = (TextView) view.findViewById(R.id.email);
        contraseña = (TextView) view.findViewById(R.id.contraseña);
        repcontraseña = (TextView) view.findViewById(R.id.reContraseña);

        if(internet.isOnlineNet()){
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference usuariosref = database.getReference(FirebaseReferences.USUARIOSINFO);
            usuariosref.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                    Usuario usuario = dataSnapshot.getValue(Usuario.class);
                    if(usuario.getEmail().equals(emailantiguo)){
                        nombre.setText(usuario.getNombre().toString());
                        apellidos.setText(usuario.getApellidos().toString());
                        email.setText(usuario.getEmail().toString());
                        contraseña.setText(usuario.getContraseña().toString());
                        repcontraseña.setText(usuario.getContraseña().toString());
                    }


                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else{
            Toast.makeText(getActivity(),"No conexion a internet disponible",Toast.LENGTH_SHORT).show();

        }



        return view;
    }
}
