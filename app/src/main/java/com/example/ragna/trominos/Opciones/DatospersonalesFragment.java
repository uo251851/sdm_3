package com.example.ragna.trominos.Opciones;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ragna.trominos.R;
import com.example.ragna.trominos.RegistrarActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import BD.FirebaseReferences;
import Logica.ConexionInternet;
import Logica.Usuario;

/**
 * Created by AlexGonzCam on 1/1/18.
 */

public class DatospersonalesFragment extends Fragment {
    private static final String TAG = "DatospersonalesFragment";

    private String email;

    private TextView username;
    private TextView numpuntuacion;
    private ImageView imagen;

    private ConexionInternet internet = new ConexionInternet();

    public DatospersonalesFragment(){}

    public DatospersonalesFragment(String email) {
        this.email = email;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.datospersonales_fragment,container,false);

        username = (TextView) view.findViewById(R.id.usuario);
        numpuntuacion = (TextView) view.findViewById(R.id.numpuntuacion);
        imagen = (ImageView) view.findViewById(R.id.imageid);

        if(internet.isOnlineNet()){
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference usuariosref = database.getReference(FirebaseReferences.USUARIOSINFO);
            usuariosref.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                    Usuario usuario = dataSnapshot.getValue(Usuario.class);
                    if(usuario.getEmail().equals(email)) {
                        username.setText(usuario.getUsername().toString());
                        numpuntuacion.setText(String.valueOf(usuario.getPuntuacion()*(-1)));
                        if(!usuario.getImg().equals("") && usuario.getImg().startsWith("content://media")){
                            Uri path= Uri.parse(usuario.getImg());
                            imagen.setImageURI(path);
                        }

                    }



                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else{
            Toast.makeText(getActivity(),"No conexion a internet disponible",Toast.LENGTH_SHORT).show();

        }


        return view;
    }



}
