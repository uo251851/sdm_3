package com.example.ragna.trominos;

import android.content.Intent;
import android.media.Image;
import android.media.MediaPlayer;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ImageButton;

import com.example.ragna.trominos.Opciones.AjustesActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import Logica.ConexionInternet;


public class ModosJuegoActivity extends AppCompatActivity {

    /**
     * Constantes para ayudar a distinguir las dificultades.
     */
    final static int FACIL = 0;
    final static int NORMAL = 1;
    final static int DIFICIL = 2;
    final static int TORNEO = 3;

    MediaPlayer mediaPlayer;
    private ImageButton btnMusica;

    private ConexionInternet internet = new ConexionInternet();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modos_juego);

        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.menu);

        mediaPlayer.start();
        mediaPlayer.setLooping(true);
        mediaPlayer.setVolume((float)0.5, (float)0.5);
    }


    public void mostrarOpcionesJuego(View view)
    {
        Intent dificultadActivity = new Intent(ModosJuegoActivity.this, DificultadActivity.class);
        dificultadActivity.putExtra("modo", "normal");
        startActivity(dificultadActivity);
    }

    public void mostrarOpcionesJuegoContrarreloj(View view)
    {
        Intent dificultadActivity = new Intent(ModosJuegoActivity.this, DificultadActivity.class);
        dificultadActivity.putExtra("modo", "contrarreloj");
        startActivity(dificultadActivity);
    }

    public void mostrarOpcionesJuegoTorneo(View view)
    {
        if(internet.isOnlineNet()){
            FirebaseUser usuarioactual = FirebaseAuth.getInstance().getCurrentUser();
            if (usuarioactual != null) {
                Intent dificultadActivity = new Intent(ModosJuegoActivity.this, PartidaActivity.class);
                dificultadActivity.putExtra("modo", "torneo");
                dificultadActivity.putExtra("dificultad", TORNEO);
                startActivity(dificultadActivity);
            }else{
                Toast.makeText(this, "Modo torneo no disponible como invitado", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(this, "No conexion a internet disponible", Toast.LENGTH_SHORT).show();
        }

    }

    public void FAB(View view){
        if(internet.isOnlineNet()){
            //Añadir comprobacion si es un usuario invitaod o usuario
            FirebaseUser usuarioactual = FirebaseAuth.getInstance().getCurrentUser();
            if (usuarioactual != null) {
                Intent opciones = new Intent(ModosJuegoActivity.this, AjustesActivity.class);
                startActivity(opciones);
            }else{
                Toast.makeText(this, "Ajustes no disponibles como invitado", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this, "No conexion a internet disponible", Toast.LENGTH_SHORT).show();
        }


    }

    public void musica(View view)
    {
        btnMusica = (ImageButton) findViewById(R.id.btnMusica);
        if(mediaPlayer.isPlaying())
        {
            mediaPlayer.pause();
            btnMusica.setImageResource(R.drawable.musicoff);
        }
        else
        {
            mediaPlayer.start();
            btnMusica.setImageResource(R.drawable.musicon);
        }

    }

    public void mostrarInfo(View view)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(R.string.instruccionesjuego)
                .setTitle(R.string.informacion);

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    @Override
    protected void onPause()
    {
        mediaPlayer.pause();
        super.onPause();
    }

    @Override
    protected void onStop()
    {
        mediaPlayer.pause();
        super.onStop();
    }

    @Override
    protected void onRestart()
    {
        if(btnMusica != null) btnMusica.setImageResource(R.drawable.musicon);
        mediaPlayer.start();
        super.onRestart();
    }

}
