package com.example.ragna.trominos;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import Logica.ConexionInternet;
import Logica.Usuario;
import Logica.Validador;
import BD.Conexion;

public class RegistrarActivity extends AppCompatActivity {


    private TextView nombre;
    private TextView apellidos;
    private TextView email;
    private TextView contraseña;
    private TextView repcontraseña;
    private TextView username;

    private ConexionInternet internet = new ConexionInternet();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

    }

    public void Ok(View view){
        nombre = (TextView) findViewById(R.id.nombre);
        apellidos = (TextView) findViewById(R.id.apellidos);
        email = (TextView) findViewById(R.id.email);
        contraseña = (TextView) findViewById(R.id.contraseña);
        repcontraseña = (TextView) findViewById(R.id.reContraseña);
        username = (TextView) findViewById(R.id.usuarioregistrar);

        Usuario usuario = new Usuario(nombre.getText().toString()
                ,apellidos.getText().toString()
                ,email.getText().toString()
                ,contraseña.getText().toString()
                ,username.getText().toString());

        Validador validador = new Validador(nombre.getText().toString()
                ,apellidos.getText().toString()
                ,email.getText().toString()
                ,contraseña.getText().toString()
                ,repcontraseña.getText().toString(),username.getText().toString());

        boolean result = validador.comprobarCampos(RegistrarActivity.this);
        if(result){

            if(internet.isOnlineNet()){
                //Meter datos a base de datos
                Conexion conn = new Conexion();
                conn.añadirUsuario(usuario);

                //Registrar nuevo usuario
                registrar(usuario.getEmail(),usuario.getContraseña());
            }else{
                Toast.makeText(RegistrarActivity.this,"No conexion a internet disponible",Toast.LENGTH_SHORT).show();
            }




        }

    }
    private void registrar(String email,String contraseña){

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,contraseña).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                //Si ejecucion correcta
                if(task.isSuccessful()){
                    //Inicia activity
                    Intent modosdejuego = new Intent(RegistrarActivity.this,ModosJuegoActivity.class);
                    startActivity(modosdejuego);
                    Toast.makeText(RegistrarActivity.this,"Usuario creado correctamente",Toast.LENGTH_SHORT).show();

                }
                else{
                    Toast.makeText(RegistrarActivity.this,"Fallo en la creacion de usuario",Toast.LENGTH_SHORT).show();

                }
            }
        });
    }
}
