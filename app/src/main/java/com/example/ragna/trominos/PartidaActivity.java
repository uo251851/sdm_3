package com.example.ragna.trominos;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import BD.Conexion;
import BD.FirebaseReferences;
import Logica.ColocadorPiezas;
import Logica.ConexionInternet;
import Logica.Lienzo;
import Logica.Usuario;
import Logica.piezas.Pieza;
import Logica.piezas.Pieza1;
import Logica.piezas.Pieza2;
import Logica.piezas.Pieza3;
import Logica.piezas.Pieza4;

public class PartidaActivity extends AppCompatActivity {

    private final int DIEZ_MIN = 600;

    private int filas;
    private int dificultad;
    private String modojuego;
    private int tiempoCountDown;

    private int width;
    private int height;

    private Lienzo lienzo;
    private ColocadorPiezas colocador;
    private RelativeLayout layoutLienzo, layoutPiezasCreadas;
    private PartidaActivity activity = this;
    private ImageView imagePieza1, imagePieza2, imagePieza3, imagePieza4;

    private boolean partidaComenzada;
    private Chronometer cronometro;
    private CountDownTimer countdown;
    private int clickPiezas;
    private int puntuacion;
    private int bonificacion;
    private int tiempoRestante;
    private long tiempoInicio;

    private Pieza piezaActual;


    private TextView txCountDown;
    private MediaPlayer mediaPlayer;
    private ImageButton btnMusica;
    public boolean blankPlaced = false;

    private ConexionInternet internet = new ConexionInternet();
    private Lienzo vista;

    /**
     * Se obtiene la dificultad y el modo de juego seleccionado.
     * Se inicia el lienzo (Canvas), las piezas (ImageViews) y
     * resto de componentes (nombre usuario y cronometro).
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partida);

        // Obtiene la dificultad del intent
        Intent intent = getIntent();
        int dificultad = intent.getIntExtra("dificultad", 0);
        this.dificultad = dificultad;
        String modojuego = intent.getStringExtra("modo");
        this.modojuego = modojuego;

        this.layoutPiezasCreadas = (RelativeLayout) findViewById(R.id.layoutPiezasCreadas);

        iniciarLienzo();
        iniciarPiezas();
        iniciarComponentes();
    }

    private void iniciarComponentes()
    {
        iniciarUsuario();
        this.partidaComenzada = false;
        this.clickPiezas = 0;
        if (modojuego.equals("normal") || modojuego.equals("contrarreloj"))
            this.mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.partida);
        else
            this.mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.torneo);
        mediaPlayer.start();
        mediaPlayer.setLooping(true);
        iniciarCronometro();
    }

    private void iniciarUsuario()
    {
        TextView txUsuario = (TextView) findViewById(R.id.txUsuario);
        if(internet.isOnlineNet()){
            FirebaseDatabase database = FirebaseDatabase.getInstance();

            DatabaseReference usuariosref = database.getReference(FirebaseReferences.USUARIOSINFO);
            usuariosref.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                    Usuario usuario = dataSnapshot.getValue(Usuario.class);
                    if(getEmail() == null)
                    {
                        txUsuario.setText("Invitado");
                    }
                    else if(usuario.getEmail().equals(getEmail())) {
                        txUsuario.setText(usuario.getUsername());
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {}

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });
        }else{
            Toast.makeText(PartidaActivity.this,"No conexion a internet disponible",Toast.LENGTH_SHORT).show();

        }

    }

    private String getEmail()
    {
        FirebaseUser usuario = FirebaseAuth.getInstance().getCurrentUser();
        if (usuario == null) return null;
        else return usuario.getEmail().toString() ;
    }

    /**
     * Se establece el formato del cronometro. En caso de estar en el modo
     * contrarreloj es necesario hacer invisible el componente Chronometer
     * y usar una textView para representar la cuenta atrás.
     */
    private void iniciarCronometro() {
        cronometro = (Chronometer) findViewById(R.id.cronometro);
        cronometro.setText("");
        cronometro.setFormat("Tiempo: %s");
        txCountDown = (TextView) findViewById(R.id.txCountDown);

        if (modojuego.equals("contrarreloj")) {
            tiempoRestante = tiempoCountDown;
            countdown = crearCuentaAtras(tiempoCountDown);
        }
        else if (modojuego.equals("torneo")) {
            ImageButton btnAyuda = (ImageButton) findViewById(R.id.btnAyuda);
            btnAyuda.setVisibility(View.INVISIBLE);
            btnAyuda.setEnabled(false);
        }
    }

    private CountDownTimer crearCuentaAtras(int tiempo)
    {
         return new CountDownTimer(tiempo, 1000) {

            public void onTick(long millisUntilFinished) {
                long tiemporestante = millisUntilFinished / 1000;
                if (tiemporestante <= 15) txCountDown.setTextColor(Color.RED);
                txCountDown.setText("Tiempo restante: " + tiemporestante);
                tiempoRestante = (int) tiemporestante;
            }

            public void onFinish() {
                txCountDown.setText("Tiempo agotado");
                tiempoRestante = 0;
                finalizarPiezas();
                finalizar();
            }
        };
    }

    private void iniciarPiezas()
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int anchoTotalLienzo = displayMetrics.widthPixels-20;

        layoutLienzo = (RelativeLayout) findViewById(R.id.lienzo);
        //int anchoTotalLienzo = layoutLienzo.getWidth()-20;
        Log.i("ancho","El ancho en las piezas:"+anchoTotalLienzo);
        width = anchoTotalLienzo/filas;
        height = anchoTotalLienzo/filas;


        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int anchoPantalla = displaymetrics.heightPixels;
        int altoPantalla = displaymetrics.widthPixels;

        RelativeLayout lienzo = (RelativeLayout) findViewById(R.id.lienzo);

        imagePieza1 = (ImageView) findViewById(R.id.imagePieza1);
        imagePieza1.setOnClickListener(new GenerarPieza());
        imagePieza2 = (ImageView) findViewById(R.id.imagePieza2);
        imagePieza2.setOnClickListener(new GenerarPieza());
        imagePieza3 = (ImageView) findViewById(R.id.imagePieza3);
        imagePieza3.setOnClickListener(new GenerarPieza());
        imagePieza4 = (ImageView) findViewById(R.id.imagePieza4);
        imagePieza4.setOnClickListener(new GenerarPieza());

        //Crea el colocador de piezas con la info del lienzo.
        colocador = new ColocadorPiezas(this, this.lienzo, filas*2);

    }

    private void finalizarPiezas()
    {
        ImageView imagePieza1 = (ImageView) findViewById(R.id.imagePieza1);
        imagePieza1.setOnClickListener(null);
        ImageView imagePieza2 = (ImageView) findViewById(R.id.imagePieza2);
        imagePieza2.setOnClickListener(null);
        ImageView imagePieza3 = (ImageView) findViewById(R.id.imagePieza3);
        imagePieza3.setOnClickListener(null);
        ImageView imagePieza4 = (ImageView) findViewById(R.id.imagePieza4);
        imagePieza4.setOnClickListener(null);
    }

    public void setPiezaActual(Pieza pieza) {
        this.piezaActual = pieza;
    }

    /**
     * Se crea un nuevo Lienzo pasandole el elemento que lo va a contener y las
     * dimensiones.
     * A continuación se añade al padre el Lienzo.
     */
    private void iniciarLienzo()
    {
        if (dificultad == ModosJuegoActivity.FACIL) { //15 segundos
            this.filas = 2;
            this.tiempoCountDown = 15000;
        }
        else if (dificultad == ModosJuegoActivity.NORMAL) //55 segundos
        {
            this.filas = 4;
            this.tiempoCountDown = 55000;
        }
        else if (dificultad == ModosJuegoActivity.DIFICIL)
        {
            this.filas = 8;
            this.tiempoCountDown = 480000;
        }
        else if (dificultad == ModosJuegoActivity.TORNEO)
        {
            this.filas = 4;
        }

        RelativeLayout lienzoLayout = (RelativeLayout) findViewById(R.id.lienzo);

        vista =new Lienzo(this, lienzoLayout, filas*2);
        lienzoLayout.addView(vista);

        this.lienzo = vista;
    }


    /**
     * Aparecen las piezas al hacer click sobre las piezas de referencia.
     * Al hacer click por primera vez comienza a funcionar el cronometro
     * o cuenta atrás.
     */
    private final class GenerarPieza implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            clickPiezas++;
            Pieza piezaNueva = null;

            switch (view.getId()) {

                case R.id.imagePieza1:
                    piezaNueva = new Pieza1(activity, layoutPiezasCreadas,layoutLienzo);
                    break;

                case R.id.imagePieza2:
                    piezaNueva = new Pieza2(activity, layoutPiezasCreadas,layoutLienzo);
                    break;

                case R.id.imagePieza3:
                    piezaNueva = new Pieza3(activity, layoutPiezasCreadas, layoutLienzo);
                    break;

                case R.id.imagePieza4:
                    piezaNueva = new Pieza4(activity, layoutPiezasCreadas, layoutLienzo);
                    break;
            }

            if (piezaActual == null)
            {
                piezaActual = piezaNueva;
                piezaActual.crearPieza(width, height);
                piezaActual.setColocador(colocador);
            }
            else if (piezaActual.getClass() != piezaNueva.getClass()) {
                piezaActual.getPieza().setVisibility(View.GONE);
                piezaActual = piezaNueva;
                piezaActual.crearPieza(width, height);
                piezaActual.setColocador(colocador);
            }

            if(!partidaComenzada)
            {
                if (modojuego.equals("normal") || modojuego.equals("torneo")) {
                    tiempoInicio = SystemClock.elapsedRealtime();
                    cronometro.setBase(SystemClock.elapsedRealtime());
                    cronometro.start();
                }
                else if (modojuego.equals("contrarreloj")) countdown.start();

                partidaComenzada = true;
                TextView txInfo = (TextView) findViewById(R.id.txInfoComenzar);
                txInfo.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Muestra un nuevo relativeLayout con un lienzo igual al de la partida.
     * El resto de componentes de la pantalla deben ser invisibles y no estar
     * disponibles.
     * @param view
     */
    public void mostrarAyuda(View view)
    {
        layoutLienzo.setVisibility(View.INVISIBLE);
        layoutPiezasCreadas.setVisibility(View.INVISIBLE);
        RelativeLayout lienzoAyuda = (RelativeLayout) findViewById(R.id.lienzoAyuda);
        lienzoAyuda.setVisibility(View.VISIBLE);

        Lienzo lienzoayuda = new Lienzo(this, lienzoAyuda, filas*2);
        lienzoAyuda.addView(lienzoayuda);

        Button btnSiguiente = (Button) findViewById(R.id.btnSiguiente);
        btnSiguiente.setEnabled(true);
        Button btnSolucion = (Button) findViewById(R.id.btnSolucion);
        btnSolucion.setEnabled(true);
        ImageView imagePieza1 = (ImageView) findViewById(R.id.imagePieza1);
        imagePieza1.setVisibility(View.INVISIBLE);
        imagePieza1.setEnabled(false);
        ImageView imagePieza2 = (ImageView) findViewById(R.id.imagePieza2);
        imagePieza2.setVisibility(View.INVISIBLE);
        imagePieza2.setEnabled(false);
        ImageView imagePieza3 = (ImageView) findViewById(R.id.imagePieza3);
        imagePieza3.setVisibility(View.INVISIBLE);
        imagePieza3.setEnabled(false);
        ImageView imagePieza4 = (ImageView) findViewById(R.id.imagePieza4);
        imagePieza4.setVisibility(View.INVISIBLE);
        imagePieza4.setEnabled(false);
        TextView textInfo = (TextView) findViewById(R.id.txInfoComenzar);
        textInfo.setVisibility(View.INVISIBLE);
    }

    public Lienzo getLienzo ()
    {
        return  this.lienzo;
    }

    private void finalizar()
    {
        Intent finalizar = new Intent(PartidaActivity.this, FinalActivity.class);
        finalizar.putExtra("dificultad", dificultad);
        finalizar.putExtra("modo", modojuego);
        finalizar.putExtra("tiemporestante",tiempoRestante);
        finalizar.putExtra("tiempototal",calcularTiempoTotal());
        startActivity(finalizar);
        finish();
    }

    private int calcularTiempoTotal()
    {
        int tiempoTotal = (int) (SystemClock.elapsedRealtime() - cronometro.getBase())/1000;
        cronometro.stop();
        return tiempoTotal;
    }

    private void calcularPuntuacion()
    {
        int tiempoTotal = calcularTiempoTotal();

        //PUNTUACION SEGUN TIEMPO
        if (tiempoTotal >= DIEZ_MIN)
            puntuacion+= 50;
        else if (tiempoTotal < DIEZ_MIN && tiempoTotal >= DIEZ_MIN/10)
            puntuacion+= 300;
        else
            puntuacion+= 500;

        //PUNTUACION SEGUN CLICKS
        if (clickPiezas <= 30)
            puntuacion+=500;
        else if (clickPiezas > 30 && clickPiezas <= 50)
            puntuacion+=300;
        else
            puntuacion+= 50;

    }

    private void calcularBonificacion()
    {
        if(puntuacion>=900)
            bonificacion+=100 + Math.random()*9+1;
        else if(puntuacion<900 && puntuacion>=450)
            bonificacion+=50 + Math.random()*9+1;
        else
            bonificacion+=10 + Math.random()*9+1;

    }

    /**
     * Metodo que se ejecuta al terminar la menu en modo torneo.
     */
    private void finalizarTorneo()
    {
        calcularPuntuacion();
        calcularBonificacion();

        Intent finalizar = new Intent(PartidaActivity.this, FinalActivity.class);
        finalizar.putExtra("dificultad", dificultad);
        finalizar.putExtra("modo", modojuego);
        finalizar.putExtra("puntos",puntuacion);
        finalizar.putExtra("bonificacion",bonificacion);
        startActivity(finalizar);
        finish();
    }

    public void finalizarPartida()
    {
        if (modojuego.equals("normal") || modojuego.equals("contrarreloj"))
            finalizar();
        else if(modojuego.equals("torneo"))
            finalizarTorneo();
    }

    @Override
    protected void onStop()
    {
        cronometro.stop();
        tiempoCountDown = tiempoRestante*1000;
        if(countdown!=null)countdown.cancel();
        mediaPlayer.pause();
        super.onStop();
    }

    @Override
    protected void onRestart()
    {
        if (modojuego.equals("normal") || modojuego.equals("torneo")) cronometro.start();
        else if (modojuego.equals("contrarreloj") && countdown!=null) {
            countdown = crearCuentaAtras(tiempoCountDown);
            countdown.start();
        }

        if(btnMusica != null) btnMusica.setImageResource(R.drawable.musicon);
        mediaPlayer.start();

        super.onRestart();
    }

    public void controlarMusica(View view)
    {
        btnMusica = (ImageButton) findViewById(R.id.btnMusicaPartida);
        if(mediaPlayer.isPlaying())
        {
            mediaPlayer.pause();
            btnMusica.setImageResource(R.drawable.musicoff);
        }
        else
        {
            mediaPlayer.start();
            btnMusica.setImageResource(R.drawable.musicon);
        }
    }

    /**
     * Si se encuentra en el layout de la ayuda vuelve a mostrar todos los elementos de la partida,
     * por el contrario, si se encuentra jugando, sale al menú principal.
     */
    @Override
    public void onBackPressed()
    {
        if (layoutLienzo.getVisibility() == View.INVISIBLE) {
            layoutLienzo.setVisibility(View.VISIBLE);
            layoutPiezasCreadas.setVisibility(View.VISIBLE);
            RelativeLayout lienzoAyuda = (RelativeLayout) findViewById(R.id.lienzoAyuda);
            lienzoAyuda.setVisibility(View.INVISIBLE);

            Button btnSiguiente = (Button) findViewById(R.id.btnSiguiente);
            btnSiguiente.setEnabled(false);
            Button btnSolucion = (Button) findViewById(R.id.btnSolucion);
            btnSolucion.setEnabled(false);
            ImageView imagePieza1 = (ImageView) findViewById(R.id.imagePieza1);
            imagePieza1.setVisibility(View.VISIBLE);
            imagePieza1.setEnabled(true);
            ImageView imagePieza2 = (ImageView) findViewById(R.id.imagePieza2);
            imagePieza2.setVisibility(View.VISIBLE);
            imagePieza2.setEnabled(true);
            ImageView imagePieza3 = (ImageView) findViewById(R.id.imagePieza3);
            imagePieza3.setVisibility(View.VISIBLE);
            imagePieza3.setEnabled(true);
            ImageView imagePieza4 = (ImageView) findViewById(R.id.imagePieza4);
            imagePieza4.setVisibility(View.VISIBLE);
            imagePieza4.setEnabled(true);
        }
        else
            finish();
    }

}
