package com.example.ragna.trominos.Opciones;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.example.ragna.trominos.MenuActivity;
import com.example.ragna.trominos.R;
import com.example.ragna.trominos.RegistrarActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import BD.Conexion;
import BD.FirebaseReferences;
import Logica.ConexionInternet;
import Logica.Usuario;
import Logica.Validador;


public class AjustesActivity extends AppCompatActivity {
    private ViewPager mViewPager;

    private ConexionInternet internet = new ConexionInternet();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opciones);


        mViewPager = (ViewPager) findViewById(R.id.container);
        setUpViewPager(mViewPager);


        TabLayout tablelayout = (TabLayout) findViewById(R.id.tabs);
        tablelayout.setupWithViewPager(mViewPager);

    }


    public void setUpViewPager(ViewPager viewPager){

        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new DatospersonalesFragment(getEmail()),"DATOS PERSONALES");
        adapter.addFragment(new RankingFragment(),"RANKING");
        adapter.addFragment(new ModificarDatosFragment(getEmail()),"MODIFICAR DATOS");
        viewPager.setAdapter(adapter);
    }



    public void cerrarSesion(View view){
        if(internet.isOnlineNet()){
            FirebaseAuth.getInstance().signOut();
            //Cierra sesion
            Intent opciones = new Intent(AjustesActivity.this, MenuActivity.class);
            startActivity(opciones);
            finish();
        }else{
            Toast.makeText(this,"No conexion a internet disponible",Toast.LENGTH_SHORT).show();

        }


    }

    public String getEmail(){
        return FirebaseAuth.getInstance().getCurrentUser().getEmail().toString();
    }

    public void cargarImagen(View view){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent.createChooser(intent,"Seleccione aplicacion"),100);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== RESULT_OK){
            Uri path = data.getData();
            ImageView imagen = (ImageView) findViewById(R.id.imageid);
            if(!path.toString().startsWith("content://media")){
                actualizarImgBd(null);
            }else{
                imagen.setImageURI(path);
                actualizarImgBd(path);
            }


        }
    }

    public void actualizarImgBd(Uri path){
        if(internet.isOnlineNet()){
            if(path!=null){
                FirebaseDatabase database = FirebaseDatabase.getInstance();

                DatabaseReference usuariosref = database.getReference(FirebaseReferences.USUARIOSINFO);
                usuariosref.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                        Usuario usuario = dataSnapshot.getValue(Usuario.class);
                        if(usuario.getEmail().equals(getEmail())) {
                            usuario.setImg(path.toString());
                            Conexion conn = new Conexion();
                            conn.añadirUsuario(usuario);
                        }



                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }else{
                Toast.makeText(AjustesActivity.this,"Seleccionar solo fotos de la galeria ",Toast.LENGTH_SHORT).show();

            }
        }else{
            Toast.makeText(AjustesActivity.this,"No conexion a internet disponible",Toast.LENGTH_SHORT).show();

        }


    }

    public void modificar(View view){
        TextView nombre = (TextView) findViewById(R.id.nombre);
        TextView apellidos = (TextView) findViewById(R.id.apellidos);
        TextView email = (TextView) findViewById(R.id.email);
        TextView contraseña = (TextView) findViewById(R.id.contraseña);
        TextView repcontraseña = (TextView) findViewById(R.id.reContraseña);

        Usuario usuario = new Usuario(nombre.getText().toString()
                ,apellidos.getText().toString()
                ,email.getText().toString()
                ,contraseña.getText().toString()
                ,"");

        Validador validador = new Validador(nombre.getText().toString()
                ,apellidos.getText().toString()
                ,email.getText().toString()
                ,contraseña.getText().toString()
                ,repcontraseña.getText().toString());

        boolean result = validador.comprobarCampos(this);
        if(result){

            //Meter datos a base de datos
            Conexion conn = new Conexion();
            conn.actualizarUsuario(usuario);
            validador.usuarioActualizadoCorrectamente(this);


        }

    }

}