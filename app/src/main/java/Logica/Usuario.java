package Logica;

import android.net.Uri;

/**
 * Created by AlexGonzCam on 31/12/17.
 */

public class Usuario {
    private String nombre;
    private String apellidos;
    private String email;
    private String contraseña;
    private String username;
    private int puntuacion=0;
    private String img="";


    public Usuario(){
    }

    public Usuario(String nombre, String apellidos, String email, String contraseña,String username) {

        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.contraseña = contraseña;
        this.username = username;

    }
    public Usuario(String nombre, String apellidos, String email, String contraseña,String username,int puntuacion) {

        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.contraseña = contraseña;
        this.username = username;
        this.puntuacion = puntuacion;

    }

    public int getPuntuacion() { return puntuacion; }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
