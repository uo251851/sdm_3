package Logica.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ragna.trominos.R;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

import Logica.Usuario;

/**
 * Created by AlexGonzCam on 2/1/18.
 */

public class UsuarioAdapter extends BaseAdapter {

    private List<Usuario> listaUsuarios;
    private Context context;
    private String email;



    public UsuarioAdapter(List<Usuario> listaUsuarios, Context context){
        this.listaUsuarios = listaUsuarios;
        this.context = context;
        email = getEmail();
    }


    @Override
    public int getCount() {
        return listaUsuarios.size();
    }

    @Override
    public Object getItem(int i) {
        return listaUsuarios.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        Usuario u = (Usuario) getItem(i);


        view  = LayoutInflater.from(context).inflate(R.layout.usuarios_list,null);

        TextView pos = (TextView) view.findViewById(R.id.pos);
        ImageView image= (ImageView) view.findViewById(R.id.imageView5);
        TextView username = (TextView) view.findViewById(R.id.usuarioregistrar);
        TextView puntos = (TextView) view.findViewById(R.id.puntos);

        pos.setText(String.valueOf(i+1));
        if(email.equals(u.getEmail())){
            if(!u.getImg().equals("")&& u.getImg().startsWith("content://media")){
                image.setImageURI(Uri.parse(u.getImg()));
            }else {
                image.setImageResource(R.drawable.usuario);
            }

        }else{
            image.setImageResource(R.drawable.usuario);

        }

        username.setText(u.getUsername());
        puntos.setText(String.valueOf(u.getPuntuacion()*(-1)));

        return view;
    }


    public String getEmail() {

        return FirebaseAuth.getInstance().getCurrentUser().getEmail().toString();

    }
}
