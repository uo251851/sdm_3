package Logica;

import android.widget.Toast;

import com.example.ragna.trominos.Opciones.AjustesActivity;
import com.example.ragna.trominos.RegistrarActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import BD.FirebaseReferences;

/**
 * Created by AlexGonzCam on 29/12/17.
 */

public class Validador {
    private String nombre;
    private String apellidos;
    private String email;
    private String contraseña;
    private String repcontraseña;
    private String username;

    public Validador(String nombre, String apellidos, String email, String contraseña, String repcontraseña,String username) {

        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.contraseña = contraseña;
        this.repcontraseña = repcontraseña;
        this.username = username;

    }

    public Validador(String nombre, String apellidos, String email, String contraseña, String repcontraseña) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.contraseña = contraseña;
        this.repcontraseña = repcontraseña;
        this.username = "nonecesario";
    }


    public boolean comprobarEspaciosVacios()
    {
        boolean result = true;

        if(nombre.isEmpty()){
            return false;
        }
        else if (apellidos.isEmpty()){
            return false;

        }
        else if (email.isEmpty()){
            return false;

        }
        else if (contraseña.isEmpty()){
            return false;

        }
        else if (repcontraseña.isEmpty()){
            return false;

        }
        else if(username.isEmpty()){
            return false;
        }

        return result;
    }

    public boolean comprobarNombre(){
        if(nombre.length()>2){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean comprobarApellidos(){
        if(apellidos.length()>4){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean comprobarEmail(){
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (email.matches(emailPattern))
        {
            return true;
        }
        else {
            return false;
        }

    }
    public boolean comprobarContraseñaTamaño(){
        if(contraseña.length()>6){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean comprobarContraseñaDigito(){
        for(int i = 0;i<contraseña.length();i++){
            if(Character.isDigit(contraseña.charAt(i))){
                return true;
            }
        }
        return false;
    }
    public boolean comprobarContraseñaLetra(){
        for(int i = 0;i<contraseña.length();i++){
            if(Character.isLetter(contraseña.charAt(i))){
                return true;
            }
        }
        return false;
    }
    public boolean comprobarContraseñas(){
        if(contraseña.equals(repcontraseña)){
            return true;
        }
        else{
            return false;
        }
    }

    public void comprobarEmailRepetido(RegistrarActivity registrarActivity){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usuariosref = database.getReference(FirebaseReferences.USUARIOSINFO);
        ChildEventListener childEventListener = usuariosref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                Usuario usuario = dataSnapshot.getValue(Usuario.class);
                if (usuario.getEmail().equals(email)) {
                    try {
                        throw new Exception();
                    } catch (Exception e) {
                    }
                }

                }

                @Override
                public void onChildChanged (DataSnapshot dataSnapshot, String s){

                }

                @Override
                public void onChildRemoved (DataSnapshot dataSnapshot){

                }

                @Override
                public void onChildMoved (DataSnapshot dataSnapshot, String s){

                }

                @Override
                public void onCancelled (DatabaseError databaseError){

                }
            });
        };

    public void comprobarUsuarioRepetido(RegistrarActivity registrarActivity) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usuariosref = database.getReference(FirebaseReferences.USUARIOSINFO);
        usuariosref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey)  {
                Usuario usuario = dataSnapshot.getValue(Usuario.class);
                if(usuario.getUsername().equals(username)){
                    try {
                        throw new Exception();
                    } catch (Exception e) {
                    }

                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

     }

    public boolean comprobarCampos(RegistrarActivity registrarActivity) {
        if(!comprobarEspaciosVacios()){
            Toast.makeText(registrarActivity,"Campos vacios",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarNombre()){
            Toast.makeText(registrarActivity,"Nombre demasiado corto",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarApellidos()){
            Toast.makeText(registrarActivity,"Apellidos demasiado cortos",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarEmail()){
            Toast.makeText(registrarActivity,"Email no valido",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarContraseñaTamaño()){
            Toast.makeText(registrarActivity,"Contraseña demasiado corta",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarContraseñaDigito()){
            Toast.makeText(registrarActivity,"Contraseña debe contener un digito",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarContraseñaLetra()){
            Toast.makeText(registrarActivity,"Contraseña debe contener una letra",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarContraseñas()){
            Toast.makeText(registrarActivity,"Contraseñas distintas",Toast.LENGTH_LONG).show();
            return false;
        }else{
            try {
                comprobarUsuarioRepetido(registrarActivity);
                comprobarEmailRepetido(registrarActivity);
            } catch (Exception e) {
                Toast.makeText(registrarActivity,"Username o email ya existentes",Toast.LENGTH_LONG).show();
                return false;
            }
            return true;
        }

    }


    public boolean comprobarCampos(AjustesActivity ajustesActivity) {
        if(!comprobarEspaciosVacios()){
            Toast.makeText(ajustesActivity,"Campos vacios",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarNombre()){
            Toast.makeText(ajustesActivity,"Nombre demasiado corto",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarApellidos()){
            Toast.makeText(ajustesActivity,"Apellidos demasiado cortos",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarEmail()){
            Toast.makeText(ajustesActivity,"Email no valido",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarContraseñaTamaño()){
            Toast.makeText(ajustesActivity,"Contraseña demasiado corta",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarContraseñaDigito()){
            Toast.makeText(ajustesActivity,"Contraseña debe contener un digito",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarContraseñaLetra()){
            Toast.makeText(ajustesActivity,"Contraseña debe contener una letra",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(!comprobarContraseñas()){
            Toast.makeText(ajustesActivity,"Contraseñas distintas",Toast.LENGTH_LONG).show();
            return false;
        }else{
            return true;
        }
    }

    public void usuarioActualizadoCorrectamente(AjustesActivity ajustesActivity) {
        Toast.makeText(ajustesActivity,"Datos actualizados",Toast.LENGTH_LONG).show();

    }
}
