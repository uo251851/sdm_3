package Logica;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.ragna.trominos.PartidaActivity;

import java.util.Random;

/**
 * Created by MarcialFrancisco on 12/01/2018.
 */

public class Lienzo extends View {
    private RelativeLayout lienzoLayout;
    private int dimension;

    //Alamacena los rectángulos dibujados para tener su referencia posteriormente
    private Rect[][] rectMatrix;

    private int xLienzo;
    private int yLienzo;

    private int distancia;
    private int marginX;

    private int firstBlankX;
    private int firstBlankY;

    private Canvas canvas;
    private PartidaActivity partidaActivity;

    public Lienzo(PartidaActivity context, RelativeLayout lienzo, int dimension) {
        super(context);
        partidaActivity = context;
        this.lienzoLayout = lienzo;
        this.dimension = dimension;
        rectMatrix = new Rect[dimension][dimension];

        int anchoTotalLienzo = lienzoLayout.getWidth()-20;
        marginX = lienzoLayout.getWidth()-anchoTotalLienzo;
        xLienzo = (int) lienzoLayout.getX()+marginX;
        yLienzo = (int) lienzoLayout.getY();

        distancia = anchoTotalLienzo/dimension-4;

    }

    /**
     * Este método se encarga de dibujar el lienzo sobre el layout.
     * Primero obtener el ancho del lienzo (hemos restado -40 para que
     * sea un poco más pequeño), la posicion x e y.
     *
     * La distancia y dimesiones de cada cuadrado será el ancho del layout
     * entre el numero de piezas por fila.
     *
     * A continuación con un doble for vamos añadiendo los cuadrados (clase
     * Rect) estableciendo la posicion y dimensiones.
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas)
    {
        int anchoTotalLienzo = lienzoLayout.getWidth()-20;
        marginX = lienzoLayout.getWidth()-anchoTotalLienzo;
        xLienzo = (int) lienzoLayout.getX()+marginX;
        yLienzo = (int) lienzoLayout.getY();

        distancia = anchoTotalLienzo/dimension-4;

        this.canvas = canvas;
        super.onDraw(canvas);

        //Creacion paintura del lienzo
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(4);
        paint.setStyle(Paint.Style.STROKE);

        //Obtención de primer cuadrante
        generateFirstBlank();

        boolean placed = false;

        for (int row = 0; row < dimension; row++) {

            for (int column = 0; column < dimension; column++)
            {
                if (row % 2 == 0) {
                    paint.setStrokeWidth(8);

                        canvas.drawLine(xLienzo+distancia*column, yLienzo+distancia*row,
                                xLienzo+distancia*dimension-1, yLienzo+distancia*row, paint);

                    paint.setStrokeWidth(4);
                }
                if (column % 2 == 0 ) {
                    paint.setStrokeWidth(8);
                    canvas.drawLine(xLienzo+distancia*column, yLienzo+distancia*row,
                            xLienzo+distancia*column, yLienzo+distancia*dimension-1, paint);
                    paint.setStrokeWidth(4);
                }

                Rect rect = new Rect();

                rect.set(xLienzo+distancia*column, yLienzo+distancia*row,
                        xLienzo+distancia*(column+1), yLienzo+distancia*(row+1));

                    canvas.drawRect(rect, paint);

                rectMatrix[row][column] = rect;

            }
        }
        paint.setStrokeWidth(8);

        //Dibujar líneas gruesas márgenes derecho e inferior del tablero.
        canvas.drawLine(xLienzo, yLienzo+distancia*dimension,
                xLienzo+distancia*dimension, yLienzo+distancia*dimension, paint);

        canvas.drawLine(xLienzo+distancia*dimension, yLienzo,
                xLienzo+distancia*dimension, yLienzo+distancia*dimension, paint);

    }

    /**
     * Generates coordinates for the first blank in the puzzle
     * Later it must be drawn as well
     */
    private void generateFirstBlank()
    {
        if (!partidaActivity.blankPlaced) {

            partidaActivity.blankPlaced = true;

            Random random = new Random();
            firstBlankX = random.nextInt(dimension);
//            firstBlankX = 7;
            firstBlankY = random.nextInt(dimension);
//            firstBlankY = 5;
        }

        Rect rect = new Rect();

        rect.set(xLienzo+distancia*firstBlankY, yLienzo+distancia*firstBlankX,
                xLienzo+distancia*(firstBlankY+1), yLienzo+distancia*(firstBlankX+1));

        //Creacion paintura del lienzo
        Paint myPaint = new Paint();
        myPaint.setColor(Color.MAGENTA);
        myPaint.setStrokeWidth(5);
        myPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        canvas.drawRect(rect, myPaint);



    }

    public Rect[][] getRectMatrix()
    {
        return rectMatrix;
    }
    public int getxLienzo()
    {
        return xLienzo;
    }
    public int getyLienzo()
    {
        return yLienzo;
    }
    public int getDimension()
    {
        return dimension;
    }
    public int getDistancia()
        {
            return distancia;
        }
    public int getMarginX()
        {
            return marginX;
        }
    public Canvas getCanvas()
        {
            return canvas;
        }

    public int getFirstBlankX() {
        return firstBlankX;
    }

    public int getFirstBlankY() {
        return firstBlankY;
    }
}
