package Logica.piezas;

import android.content.res.Resources;
import android.support.constraint.ConstraintLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.ragna.trominos.PartidaActivity;
import com.example.ragna.trominos.R;

import Logica.ColocadorPiezas;

/**
 * Created by AlexGonzCam on 28/12/17.
 */

public class Pieza4 extends Pieza {
    PartidaActivity pa;
    public Pieza4(PartidaActivity partidaActivity, RelativeLayout layoutPiezasCreadas, RelativeLayout layoutLienzo) {
        super(partidaActivity, layoutPiezasCreadas, layoutLienzo);
        pa = partidaActivity;
    }

    @Override
    protected void asignarPieza(ImageView pieza) {

        pieza.setImageResource(R.drawable.pieza4);
    }

    @Override
    protected Pieza crearNuevaPieza(int width, int height) {
        Pieza p = new Pieza4(pa, super.layoutPiezasCreadas, super.layoutLienzo);
        p.crearPieza(super.width, super.height);
        return p;
    }

    @Override
    protected Pieza crearNuevaPiezaConColocador(int width, int height, ColocadorPiezas cP) {
        Pieza p = new Pieza4(pa, super.layoutPiezasCreadas, super.layoutLienzo);
        p.setColocador(cP);
        p.crearPieza(width, height);
        return p;
    }
}
