package Logica.piezas;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.ragna.trominos.PartidaActivity;
import com.example.ragna.trominos.R;

import Logica.ColocadorPiezas;
import Logica.Lienzo;

/**
 * Created by AlexGonzCam on 28/12/17.
 */

public abstract class Pieza {

    private PartidaActivity partidaActivity;
    public RelativeLayout layoutPiezasCreadas;
    public RelativeLayout layoutLienzo;

    private ViewGroup rootLayout;
    //private int _xDelta;
    //private int _yDelta;
    private float _xDelta, _yDelta;

    public int width;
    public int height;

    public boolean inLienzo = false;

    private ImageView pieza;


    private ColocadorPiezas colocador;

    private int coordTableroX ;
    private int coordTableroY ;

    private float coordX;
    private float coordY;

    public boolean newPieza = true;


    public Pieza(PartidaActivity partidaActivity, RelativeLayout layoutPiezasCreadas, RelativeLayout layoutLienzo) {

        this.layoutPiezasCreadas = layoutPiezasCreadas;
        this.partidaActivity = partidaActivity;
        this.layoutLienzo = layoutLienzo;
        this.rootLayout = (ViewGroup) this.partidaActivity.findViewById(R.id.view_partida);

    }

    public Pieza(PartidaActivity pA, RelativeLayout layoutPiezasCreadas, RelativeLayout
            layoutLienzo, ColocadorPiezas cP) {

        this(pA, layoutPiezasCreadas, layoutLienzo);
        setColocador(cP);

    }

    public ImageView getPieza() {
        return pieza;
    }

    public ColocadorPiezas getColocador ()
    {
        return  colocador;
    }

    public void setColocador (ColocadorPiezas cP)
    {
        this.colocador = cP;
    }

    public int getCoordTableroX() {
        return coordTableroX;
    }

    public int getCoordTableroY() {
        return coordTableroY;
    }

    public void setCoordTableroX(int coordTableroX) {
        this.coordTableroX = coordTableroX;
    }

    public void setCoordTableroY(int coordTableroY) {
        this.coordTableroY = coordTableroY;
    }

    public float getCoordX() {
        return coordX;
    }

    public float getCoordY() {
        return coordY;
    }

    public void setCoordX(float coordX) {
        this.coordX = coordX;
    }

    public void setCoordY(float coordY) {
        this.coordY = coordY;
    }

    public void crearPieza(int width, int height){
        //Creacion de imagen
        pieza = new ImageView(partidaActivity);

        //Asignar tipo de pieza(1,2,3 o 4)
        asignarPieza(pieza);

        //Guarda anchura y altura
        this.width = width;
        this.height = height;

        //Asignamos tamaño a la pieza
        //RelativeLayout relativeLayout = (RelativeLayout) partidaActivity.findViewById(R.id.view_partida);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(width, height);
        pieza.setLayoutParams(lp);

        //Asignar posicion dependiendo del tipo de pieza(1,2,3 o 4)
        asignarPosicion(lp);

        //Añadir pieza a la activity
        //relativeLayout.addView(pieza, lp);
        layoutPiezasCreadas.addView(pieza, lp);

        //Asignamos movimiento a la pieza
        pieza.setOnTouchListener(new MoverPieza(this));


    }

    protected void asignarPosicion(RelativeLayout.LayoutParams lp) {
        if (width >= layoutLienzo.getWidth()/4) {
            lp.leftMargin = layoutLienzo.getWidth() / 4;
            lp.topMargin = (rootLayout.getHeight()/2) + (rootLayout.getHeight()/5);
        }
        else if (width >= layoutLienzo.getWidth()/8)
        {
            lp.leftMargin = layoutLienzo.getWidth()/3;
            lp.topMargin = (rootLayout.getHeight()/2) + (rootLayout.getHeight()/4);
        }
        else
        {
            lp.leftMargin = rootLayout.getWidth()/2 - width;
            lp.topMargin = (rootLayout.getHeight()/2) + (rootLayout.getHeight()/3);
        }

    }

    protected abstract void asignarPieza(ImageView pieza);

    protected abstract Pieza crearNuevaPieza(int width, int height);

    protected abstract Pieza crearNuevaPiezaConColocador(int width, int height,ColocadorPiezas cP);


    /**
     * Obtiene el colocador de la pieza y lo invoca, pasando a la propia instancia de pieza como
     * param.
     */
    protected void colocarPieza (View view)
    {
        getColocador().colocar(this, view);

    }

    private final class MoverPieza implements View.OnTouchListener {

        //Referencia a la pieza, por si se activa su colocación el en el juego.
        Pieza p;

        public MoverPieza (Pieza p)
        {
            this.p = p;
        }

        /**
         * IMPORTANTE: actualizado forma de movimientos de las piezas
         * la posición de la nueva pieza es la correcta y funciona mucho
         * mejor.
         * @param view
         * @param event
         * @return
         */


        @Override
        public boolean onTouch(View view, MotionEvent event) {

            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    p.getColocador().quitarPieza(p);

                    view.bringToFront();
                    p.setCoordX(view.getX());
                    p.setCoordY(view.getY());


                    _xDelta = view.getX() - event.getRawX();
                    _yDelta = view.getY() - event.getRawY();

                    break;

                case MotionEvent.ACTION_MOVE:

//                    p.getColocador().quitarPieza(p);

                    view.animate()
                            .x(event.getRawX() + _xDelta)
                            .y(event.getRawY() + _yDelta)
                            .setDuration(0)
                            .start();
                    break;

                case MotionEvent.ACTION_UP:

                    partidaActivity.setPiezaActual(null);
                    p.newPieza = false;


                    int[] location = new int[2];
                    view.getLocationOnScreen(location);

                    if (!checkInLienzo(location[0], location[1])){

                        if (inLienzo)
                        {
                            Toast.makeText(partidaActivity.getApplicationContext(), "Pieza eliminada",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(partidaActivity.getApplicationContext(), "Fuera de tablero",
                                    Toast.LENGTH_SHORT).show();
                        }

                        inLienzo = false;

                        view.setVisibility(View.GONE);
                    }

                    else {
                        if (!inLienzo)
                            inLienzo = true;
                        p.colocarPieza(view);
                    }


                    break;


                default:
                    return false;
            }
            return true;
        }


        public boolean checkInLienzo(float x, float y)
        {
            Lienzo lienzo = partidaActivity.getLienzo();
            int[] loc = new int[2];
            y = y -layoutLienzo.getY();
            lienzo.getLocationOnScreen(loc);

            int distancia = lienzo.getDistancia();
            int dimension = lienzo.getDimension();
            double distMultiplierX = 1.5;
            double distMultiplierY = 1.4;
            if (dimension == 16) {
                distMultiplierY = 0.4;
            }

            if (dimension == 8)
                distMultiplierY = 0.7;




            if (x + lienzo.getMarginX()*2.3< lienzo.getxLienzo() || y - (lienzo.getMarginX()*1.8) < lienzo.getyLienzo()||
                    x + distancia*distMultiplierX > lienzo.getxLienzo()+lienzo.getDistancia()*lienzo.getDimension() ||
                    y + distancia*distMultiplierY > lienzo.getyLienzo()+lienzo.getDistancia()*lienzo.getDimension()){
                return false;
            }

            return true;


        }

    }

}
