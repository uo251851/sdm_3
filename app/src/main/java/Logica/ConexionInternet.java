package Logica;

/**
 * Created by AlexGonzCam on 15/1/18.
 */

public class ConexionInternet {
    public ConexionInternet(){

    }
    public Boolean isOnlineNet() {

        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val           = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
