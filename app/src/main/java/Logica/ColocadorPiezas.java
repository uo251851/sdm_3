package Logica;

import android.graphics.Point;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;

import com.example.ragna.trominos.PartidaActivity;

import Logica.piezas.Pieza;
import Logica.piezas.Pieza1;
import Logica.piezas.Pieza2;
import Logica.piezas.Pieza3;
import Logica.piezas.Pieza4;

/**
 * Created by Ragna on 12-Jan-18.
 */

public class ColocadorPiezas{

    public final static int DURACION_ANIMACION = 300;

    private Lienzo lienzo;
    private Tile[][] tiles;

    private Point[][] vertex;

    private  int dim;
    TranslateAnimation anim;

    private int distancia;

    private boolean vertexCreated = false;

    private boolean tilesCreated = false;

    private PartidaActivity partidaActivity;

    private Pieza p;


    public  ColocadorPiezas (Lienzo lienzo, int dim)
    {
        this.lienzo =  lienzo;
        this.tiles = new Tile[dim][dim];
        this.dim = dim;
        vertex = new Point[dim+1][dim+1];
    }

    public  ColocadorPiezas (PartidaActivity pA, Lienzo lienzo, int dim)
    {
        this.lienzo =  lienzo;
        this.tiles = new Tile[dim][dim];
        this.dim = dim;
        vertex = new Point[dim+1][dim+1];
        this.partidaActivity = pA;

    }


    /**
     * Con la información del lienzo almacenada, recibe una pieza, mira su tipo, y la pinta en el
     * lienzo usando la información de la misma.
     * @param pieza Pieza a colocar.
     */
    public void colocar (Pieza pieza, View view)
    {
        this.p = pieza;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        partidaActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int anchoTotalLienzo = displayMetrics.widthPixels-20;
        distancia = anchoTotalLienzo/dim;

        boolean animate = true;


        if (!vertexCreated)
            createVertexMatrix();

        if (!tilesCreated)
            createTilesMatrix();

        int[] piezaCoordinates = new int[2];
        view.getLocationInWindow(piezaCoordinates);

        //Coordenadas en pantalla de la pieza.
        int originalX = piezaCoordinates[0];
        int originalY = piezaCoordinates[1] + lienzo.getyLienzo();


        int movAnimationX = 0;
        int movAnimationY = 0;

        Point correctPoint;

        int[] correctPointCoords = findClosestVertexCoordsInMatrix(originalX,originalY);
        int i = correctPointCoords[0];
        int j = correctPointCoords[1];

        int [] piezaRectCoords;



        if (p instanceof Pieza1)
        {
            //FIX
            if (dim == 4)
            {
                if (i == vertex[0].length-1)
                    i-=1;
            }


            piezaRectCoords = findTileOfPieza (i, j);
            int iTile = piezaRectCoords[0];
            int jTile = piezaRectCoords[1];

            checkProfundidadPieza (i, j, view);

            //FIX
            if (dim == 8)
            {
                if (iTile == dim-1)
                    iTile-=1;
            }

            if (dim == 16)
            {
                iTile+=2;
            }

            p.setCoordTableroX(iTile);
            p.setCoordTableroY(jTile);

            //Lógica
            if (puedeRecibirPieza(p, p.getCoordTableroX(), p.getCoordTableroY()))
            {
                if (dim == 16 && i == 18) {
                    i -= 2;
                    movAnimationY += lienzo.getDistancia()*2;
                    iTile -=2;
                }
                else if (dim == 16 && i == 17)
                {
                    i -= 1;
                    movAnimationY += lienzo.getDistancia();
                    iTile-=1;
                }
                tiles[iTile][jTile].asignarPieza(p);
                tiles[iTile][jTile+1].asignarPieza(p);
                tiles[iTile + 1][jTile + 1].asignarPieza(p);

                //UI


                correctPoint = vertex [i][j];

                movAnimationX += correctPoint.x - originalX;
                movAnimationY += correctPoint.y - originalY;

            }
            else {
                animate = false; //No puede recibir pieza -> no se anima nada
            }

        }
        else if (p instanceof Pieza2)
        {

            //FIX
            if (dim == 4)
            {
                if (i == vertex[0].length-1)
                    i-=1;

                if (j == vertex[0].length-1)
                    j-=1;
            }

            piezaRectCoords = findTileOfPieza (i, j);
            int iTile = piezaRectCoords[0];
            int jTile = piezaRectCoords[1];

            checkProfundidadPieza (i, j, view);


            //FIX
            if (dim == 8)
            {
                if (iTile == dim-1)
                    iTile-=1;
            }

            if (dim == 16)
            {
                iTile+=2;
            }


            p.setCoordTableroX(iTile);
            p.setCoordTableroY(jTile);
            //Lógica
            if (puedeRecibirPieza(p,p.getCoordTableroX(), p.getCoordTableroY()))
            {
                if (dim == 16 && i == 18) {
                    i -= 2;
                    movAnimationY += lienzo.getDistancia()*2;
                    iTile -=2;
                }
                else if (dim == 16 && i == 17)
                {
                    i -= 1;
                    movAnimationY += lienzo.getDistancia();
                    iTile-=1;
                }
                tiles[iTile][jTile].asignarPieza(p);
                tiles[iTile+1][jTile].asignarPieza(p);
                tiles[iTile][jTile+1].asignarPieza(p);

                //UI

                correctPoint = vertex [i][j];

                movAnimationX += correctPoint.x - originalX;
                movAnimationY += correctPoint.y - originalY;

            }
            else {
                animate = false; //No puede recibir pieza -> no se anima nada
            }

        }
        else if (p instanceof Pieza3)
        {
            //FIX
            if (dim == 4)
            {
                if (i == vertex[0].length-1)
                    i-=1;

                if (i == 0)
                    i+=1;

                if (j == vertex[0].length-1)
                    j-=1;
            }


            piezaRectCoords = findTileOfPieza (i, j);
            int iTile = piezaRectCoords[0];
            int jTile = piezaRectCoords[1];

            Log.i("Coordenadas pieza3", iTile + ", " + jTile + " <-- Casilla de la pieza en tablero PRE FIXES en Colocar");


            checkProfundidadPieza (i, j, view);

            //FIX
            if (dim == 8)
            {
                if (iTile == dim-1)
                    iTile-=1;
            }

            if (dim == 16)
            {
                iTile+=2;
            }

            Log.i("Coordenadas pieza3", iTile + ", " + jTile + " <-- Casilla de la pieza en tablero POST FIXES en Colocar, y coordenadas setteadas de la pieza");

            p.setCoordTableroX(iTile);
            p.setCoordTableroY(jTile);
            //Lógica
            if (puedeRecibirPieza(p, p.getCoordTableroX(), p.getCoordTableroY()))
            {
                if (dim == 16 && i == 18) {
                    i -= 2;
                    movAnimationY += lienzo.getDistancia()*2;
                    iTile -=2;
                }
                else if (dim == 16 && i == 17)
                {
                    i -= 1;
                    movAnimationY += lienzo.getDistancia();
                    iTile-=1;
                }
                tiles[iTile][jTile].asignarPieza(p);
                tiles[iTile+1][jTile].asignarPieza(p);
                tiles[iTile+1][jTile+1].asignarPieza(p);

                //UI

                correctPoint = vertex [i][j];

                movAnimationX += correctPoint.x - originalX;
                movAnimationY += correctPoint.y - originalY;


            }
            else {
                animate = false; //No puede recibir pieza -> no se anima nada
            }



        }
        else if (p instanceof Pieza4)
        {
            //FIX
            if (dim == 4)
            {
                if (i == vertex[0].length-1)
                    i-=1;

                if (i == 0)
                    i+=1;

                if (j == vertex[0].length-1)
                    j-=1;
            }


            piezaRectCoords = findTileOfPieza (i, j);
            int iTile = piezaRectCoords[0];
            int jTile = piezaRectCoords[1];

            Log.i("Coordenadas pieza4", iTile + ", " + jTile + " <-- Casilla de la pieza en tablero PRE FIXES en Colocar");


            checkProfundidadPieza (i, j, view);


            //FIX
            if (dim == 8)
            {
                if (iTile == dim-1)
                    iTile-=1;
            }

            if (dim == 16)
            {
                iTile+=2;
            }

            Log.i("Coordenadas pieza4", iTile + ", " + jTile + " <-- Casilla de la pieza en tablero POST FIXES en Colocar, y coordenadas setteadas de la pieza");
//si coordX son 14, reducirlo en 2 (porque lo pusiste en el 12)
            p.setCoordTableroX(iTile);
            p.setCoordTableroY(jTile);
            //Lógica
            if (puedeRecibirPieza(p, p.getCoordTableroX(), p.getCoordTableroY()))
            {
                if (dim == 16 && i == 18) {
                    i -= 2;
                    movAnimationY += lienzo.getDistancia()*2;
                    iTile -=2;
                }
                else if (dim == 16 && i == 17)
                {
                    i -= 1;
                    movAnimationY += lienzo.getDistancia();
                    iTile-=1;
                }
                tiles[iTile+1][jTile].asignarPieza(p);
                tiles[iTile][jTile+1].asignarPieza(p);
                tiles[iTile+1][jTile+1].asignarPieza(p);

                //UI

                correctPoint = vertex [i][j];

                movAnimationX += (correctPoint.x-originalX) ;
                movAnimationY += (correctPoint.y-originalY) ;

            }
            else {

                animate = false; //No puede recibir pieza -> no se anima nada

            }
        }

        if (animate) {

            //Pasar la view y las coordenadas X e Y a ajustar de la pieza
            TranslateAnimation animation = getAnimation(view, movAnimationX, movAnimationY);

            view.startAnimation(animation);



            checkGanador();
        }
        else
        {
            Toast.makeText(partidaActivity.getApplicationContext(), "Solapada", Toast.LENGTH_SHORT).show();
            view.setVisibility(View.GONE);
//            TranslateAnimation animation = getAnimation(view, -(int)view.getX()+(int)p.getCoordX(), -(int)view.getY()+(int)p.getCoordY());
//
//            view.startAnimation(animation);
//
//            colocar(p,view);
        }

    }


    /**
     * Crea una animación de movimiento para una view dada, desplazandola unas coordenadas X e Y
     * dadas.

     */
    private TranslateAnimation getAnimation (View view, int moveX, int moveY)
    {

        anim = new TranslateAnimation(0, moveX, 0, moveY);
        anim.setDuration(DURACION_ANIMACION);

        anim.setAnimationListener(new TranslateAnimation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) { }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                view.setX(view.getX()+moveX);
                view.setY(view.getY()+moveY);
            }
        });


        return anim;
    }


    /**
     * Registrar las coordenadas de cada vértice del tablero.
     */
    public Point[][] createVertexMatrix ()
    {

        Rect [][] rects = lienzo.getRectMatrix();

        for (int i = 0; i< rects[0].length ; i++) {
            for (int j = 0; j < rects[0].length ; j++) {

                vertex[i][j] = new Point(rects[i][j].left, rects[i][j].top);
                vertex[i+1][j] = new Point(rects[i][j].left, rects[i][j].bottom);
                vertex[i][j+1] = new Point(rects[i][j].right, rects[i][j].top);
                vertex[i+1][j+1] = new Point(rects[i][j].right, rects[i][j].bottom);

            }
        }

        vertexCreated = true;
        return vertex;
    }


    /**
     * Dadas las coordenadas de una pieza puesta en el tablero, busca cual es el vértice del
     * tablero más cercano en el que encaja, es decir, donde querría haberla puesto el usuario.

     */
    public int[] findClosestVertexCoordsInMatrix(int x, int y)
    {
        double distance = Integer.MAX_VALUE;
        int[] ret = new int[2];

        for (int i = 0; i<vertex[0].length; i++) {
            for (int j = 0; j < vertex[0].length; j++) {
                double nuevaDistancia = calcularDistanciaEntrePuntos
                        (new Point(x,y), vertex[i][j]);
                if (nuevaDistancia < distance) {
                    distance = nuevaDistancia;
                    ret[0] = i;
                    ret[1] = j;
                }
            }
        }
        if (dim == 16)
        {
            int preTop = lienzo.getyLienzo()+lienzo.getDistancia()*12;
            int top = lienzo.getyLienzo()+lienzo.getDistancia()*13 - lienzo.getDistancia()/5;

            y = (y-lienzo.getDistancia()*5 );

            if (y > preTop)
                ret[0] += 1;

            if (y > top)
                ret[0]+=1;

        }


//        if (dim == 16 && ret[0] == 16)
//            ret[0]+=2;
        return ret;

    }

    private double calcularDistanciaEntrePuntos (Point posView, Point posVertex)
    {
        double diffX = posVertex.x - posView.x;
        double diffY = posVertex.y - posView.y;

        double ret =  Math.sqrt(diffX*diffX + diffY*diffY);

        return ret;

    }

    public void createTilesMatrix()
    {
        Rect[][] rects = lienzo.getRectMatrix();
        for (int i = 0; i< rects[0].length ; i++) {
            for (int j = 0; j < rects[0].length ; j++) {

                tiles[i][j] = new Tile(rects[i][j]);

            }
        }
        tiles[lienzo.getFirstBlankX()][lienzo.getFirstBlankY()].setFilled(true);


        tilesCreated = true;
    }

    /**
     * Recibe las coordenadas del punto al que se engancha la pieza al recolocarse, de donde deduce
     * el cuadrado que ocupa la pieza.
     * Devuelvelas coordenadas en la matriz de Tiles que corresponde al Rect adecuado.
     */
    private int[] findTileOfPieza (int i, int j)
    {
        j+=1; //Fix error de implementación

        int[] ret = new int[2];
        ret[0] = i-1;
        ret[1] = j-1;

        if (dim == 8)
            ret[0]-=1;

        if (dim == 16)
        {
            ret[0] -= 5;
            if (i == vertex[0].length-1)
                    ret[0]+=2;
        }

        return ret;

    }


    public boolean puedeRecibirPieza (Pieza p, int iTile, int jTile)
    {
        if (dim == 16)
        {
            int preTop = lienzo.getyLienzo()+lienzo.getDistancia()*12;
            int top = lienzo.getyLienzo()+lienzo.getDistancia()*13 - lienzo.getDistancia()/5;

            int y = (int) (p.getCoordY()-lienzo.getDistancia()*5);

            if (y > preTop)
                iTile-=1;

            if (y > top)
                iTile-=1;

        }

        if (p instanceof Pieza1)
        {
            if (tiles[iTile][jTile].isFilled()||tiles[iTile][jTile+1].isFilled()||
                    tiles[iTile + 1][jTile + 1].isFilled())
                return false;
        }

        else if (p instanceof Pieza2)
        {

            if (tiles[iTile][jTile].isFilled()||tiles[iTile + 1][jTile].isFilled()||
                    tiles[iTile][jTile + 1].isFilled())
                return false;
        }
        else if (p instanceof Pieza3)
        {
            if (tiles[iTile][jTile].isFilled()||tiles[iTile + 1][jTile].isFilled()||
                    tiles[iTile + 1][jTile + 1].isFilled())
                return false;
        }
        else if (p instanceof Pieza4)
        {
            if (tiles[iTile][jTile+1].isFilled()||tiles[iTile + 1][jTile].isFilled()||
                    tiles[iTile + 1][jTile + 1].isFilled())
                return false;
        }

        return true;
    }

    public boolean quitarPieza (Pieza p)
    {
        if (p.newPieza)
            return true;
        try {
            if (p instanceof Pieza1) {
                tiles[p.getCoordTableroX()][p.getCoordTableroY()].desasignarPieza();
                tiles[p.getCoordTableroX()][p.getCoordTableroY() + 1].desasignarPieza();
                tiles[p.getCoordTableroX() + 1][p.getCoordTableroY() + 1].desasignarPieza();
            } else if (p instanceof Pieza2) {
                tiles[p.getCoordTableroX()][p.getCoordTableroY()].desasignarPieza();
                tiles[p.getCoordTableroX()][p.getCoordTableroY() + 1].desasignarPieza();
                tiles[p.getCoordTableroX() + 1][p.getCoordTableroY()].desasignarPieza();
            } else if (p instanceof Pieza3) {
                tiles[p.getCoordTableroX()][p.getCoordTableroY()].desasignarPieza();
                tiles[p.getCoordTableroX() + 1][p.getCoordTableroY()].desasignarPieza();
                tiles[p.getCoordTableroX() + 1][p.getCoordTableroY() + 1].desasignarPieza();
            } else if (p instanceof Pieza4) {
                tiles[p.getCoordTableroX() + 1][p.getCoordTableroY()].desasignarPieza();
                tiles[p.getCoordTableroX()][p.getCoordTableroY() + 1].desasignarPieza();
                tiles[p.getCoordTableroX() + 1][p.getCoordTableroY() + 1].desasignarPieza();
            }
        }
        catch (NullPointerException e)
        {
            try {
                tiles[lienzo.getFirstBlankX()][lienzo.getFirstBlankY()].setFilled(true);
            }catch (NullPointerException e2)
            {
                return true;
            }

            return true;
        }
        Log.i("Coordenadas pieza", p.getCoordTableroX() + ", " + p.getCoordTableroY() + " <-- Coordenadas de la pieza desasignada de lógica");

        tiles[lienzo.getFirstBlankX()][lienzo.getFirstBlankY()].setFilled(true);

        return true;

    }

    public boolean checkGanador ()
    {
        boolean win = true;
        for (int i = 0 ; i< tiles[0].length; i++)
            for (int j = 0 ; j< tiles[0].length; j++)
            {
                if (!tiles[i][j].isFilled())
                    win = false;
            }

        if (win)
            partidaActivity.finalizarPartida();
        return win;

    }

    public void checkProfundidadPieza (int x, int y, View view)
    {
        if (dim == 4)
            y= y+1;
        if (dim == 8) {
            x -= 1;
            y+=1;
        }

        final ViewGroup parent = (ViewGroup)view.getParent();


        if (x % 2 != 0 && y % 2 != 0)
            if (parent != null)
            {
                parent.removeView(view);
                parent.addView(view, parent.getChildCount()-1);
            }



    }

}