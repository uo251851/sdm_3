package Logica;

import android.graphics.Rect;
import android.view.View;

import Logica.piezas.Pieza;

/**
 * Created by Ragna on 12-Jan-18.
 * Representa cada una de las casillas del tablero. Alamcena si está rellena o no y una referencia
 * a la figura que la rellena (null si no lo está).
 */

public class Tile {

    private Rect rect;

    private boolean filled;

    private Pieza pieza;

    private View view;


    public Rect getRect() {
        return rect;
    }

    public boolean isFilled() {
        return filled;
    }

    public Pieza getPieza() {
        return pieza;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public void setPieza(Pieza pieza) {
        this.pieza = pieza;
    }

    public View getView() {
        return view;
    }

    public void setView(View v) {
        view = v;
    }

    public Tile ()
    {
    }

    public Tile (Rect rect)
    {
        setRect(rect);
        this.filled = false;

    }


    public Tile (Rect rect, boolean filled, Pieza pieza)
    {
        this(rect);
        setFilled(filled);
        setPieza(pieza);
    }

    public void asignarPieza (Pieza p)
    {
        setPieza(p);
        setFilled(true);
    }

    public void desasignarPieza ()
    {
        setPieza(null);
        setFilled(false);
    }




}
